from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

from .utils.api_enterkomputer import get_drones, get_soundcards, get_opticals


response = {}


def index(request):
    # redirects the user to the login page if he/she is not logged in.
    # Otherwise, redirect profile page.

    print('#==> masuk index')
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('lab-9:profile'))
    else:
        html = 'lab_9/session/login.html'
        return render(request, html, response)


def set_data_for_session(response, request):
    # Sets user data to session, including drones data

    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['drones'] = get_drones().json()
    response['soundcards'] = get_soundcards().json()
    response['opticals'] = get_opticals().json()

    '''if 'drones' in request.session.keys():
        response['fav_drones'] = request.session['drones']
    else:
        response['fav_drones'] = []'''

    for item in ['drones', 'soundcards', 'opticals']:
        if item in request.session.keys():
            response['fav_' + item] = request.session[item]
        else:
            response['fav_' + item] = []


def profile(request):
    # Sets user data to session (if user has any), then render to html

    print('#==> profile')

    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('lab-9:index'))

    set_data_for_session(response, request)

    html = 'lab_9/session/profile.html'
    return render(request, html, response)


# ======================================================================================================================
#
# DRONES


def add_session_drones(request, id):
    session_keys = request.session.keys()
    if 'drones' not in session_keys:
        print('# init drones')
        request.session['drones'] = [id]
    else:
        drones = request.session['drones']
        print('# existing drones => ', drones)
        if id not in drones:
            print('# add new item, then save to session')
            drones.append(id)
            request.session['drones'] = drones

    messages.success(request, 'Berhasil tambah drone favorite')
    return HttpResponseRedirect(reverse('lab-9:profile'))


def del_session_drones(request, id):
    print('# DELETE drones')
    drones = request.session['drones']
    print('before = ', drones)
    drones.remove(id)
    request.session['drones'] = drones
    print('after = ', drones)

    messages.success(request, 'Berhasil hapus drone dari favorite')
    return HttpResponseRedirect(reverse('lab-9:profile'))


def clear_session_drones(request):
    if 'drones' in request.session:
        print('# CLEAR session drones')
        print('before = ', request.session['drones'])
        del request.session['drones']

        messages.success(request, 'Berhasil reset favorite drones')
    return HttpResponseRedirect(reverse('lab-9:profile'))


# ======================================================================================================================
#
# GENERAL FUNCTIONS TO ADD, DELETE, AND CLEAR ITEMS BESIDE DRONES


def add_session_item(request, key, id):
    print('#ADD session item')
    session_key = request.session.keys()
    if key not in session_key:
        request.session[key] = [id]
    else:
        items = request.session[key]
        if id not in items:
            items.append(id)
            request.session[key] = items
    messages.success(request, "Berhasil tambah " + key + " favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))


def del_session_item(request, key, id):
    print('#DEL session item')
    items = request.session[key]
    print('before = ', items)
    items.remove(id)
    request.session[key] = items
    print('after = ', items)
    messages.success(request, "Berhasil hapus item " + key + " dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))


def clear_session_item(request, key):
    if key in request.session:
        del request.session[key]
        messages.success(request, "Berhasil hapus session : favorite " + key)
    return HttpResponseRedirect(reverse('lab-9:profile'))


# ======================================================================================================================
#
# COOKIES


def is_login(request):
    return 'user_login' in request.COOKIES and 'user_password' in request.COOKIES


def my_cookie_auth(in_username, in_password):
    # WARNING: JANGAN PAKE SSO UI
    my_username = 'ZoneZ'  # edit username yang diinginkan
    my_password = 'shakalakaboomboom'  # edit password yang diinginkan
    return in_username == my_username and in_password == my_password


def cookie_login(request):
    print('#==> masuk login')
    if is_login(request):
        return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
    else:
        html = 'lab_9/cookie/login.html'
        return render(request, html, response)


def cookie_auth_login(request):
    print('# Auth login')
    if request.method == 'POST':
        user_login = request.POST['username']
        user_password = request.POST['password']

        if my_cookie_auth(user_login, user_password):
            print('# SET cookies')
            resp = HttpResponseRedirect(reverse('lab-9:cookie_login'))
            resp.set_cookie('user_login', user_login)
            resp.set_cookie('user_password', user_password)
            return resp
        else:
            messages.error(request, 'Username atau password salah')
            return HttpResponseRedirect(reverse('lab-9:cookie_login'))


def cookie_profile(request):
    print('# cookie profile')
    if not is_login(request):
        print('Belum login')
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        in_username = request.COOKIES['user_login']
        in_password = request.COOKIES['user_password']

        if my_cookie_auth(in_username, in_password):
            html = 'lab_9/cookie/profile.html'
            return render(request, html, response)
        else:
            print('# Login dahulu')
            messages.error(request, 'Anda tidak punya akses')
            html = 'lab_9/cookie/login.html'
            return render(request, html, response)


def cookie_clear(request):
    resp = HttpResponseRedirect('/lab-9/cookie/login')
    resp.delete_cookie('lang')
    resp.delete_cookie('user_login')
    resp.delete_cookie('user_password')

    messages.info(request, 'Berhasil logout. Cookies di-reset')
    return resp 