var themes = [
    {"id":0, "text":"Red", "bcgColor":"#F44336", "fontColor":"#FAFAFA"},
    {"id":1, "text":"Pink", "bcgColor":"#E91E63", "fontColor":"#FAFAFA"},
    {"id":2, "text":"Purple", "bcgColor":"#9C27B0", "fontColor":"#FAFAFA"},
    {"id":3, "text":"Indigo", "bcgColor":"#3F51B5", "fontColor":"#FAFAFA"},
    {"id":4, "text":"Blue", "bcgColor":"#2196F3", "fontColor":"#212121"},
    {"id":5, "text":"Teal", "bcgColor":"#009688", "fontColor":"#212121"},
    {"id":6, "text":"Lime", "bcgColor":"#CDDC39", "fontColor":"#212121"},
    {"id":7, "text":"Yellow", "bcgColor":"#FFEB3B", "fontColor":"#212121"},
    {"id":8, "text":"Amber", "bcgColor":"#FFC107", "fontColor":"#212121"},
    {"id":9, "text":"Orange", "bcgColor":"#FF5722", "fontColor":"#212121"},
    {"id":10, "text":"Brown", "bcgColor":"#795548", "fontColor":"#FAFAFA"}
];

localStorage.setItem('themes', JSON.stringify(themes));

var retrievedTheme = localStorage.getItem('themes');

$(document).ready(function () {

    if (localStorage.getItem('selectedTheme')) {
        $('body').css({
            'background': JSON.parse(localStorage.getItem('selectedTheme')).bcgColor
        })
    }

    $('.chat-text').keypress(function (e) {
        var pressedKey = e.which;
        var textArea = $('.chat-text textarea');
        var message = $.trim(textArea.val());
        if (pressedKey === 13 && !e.shiftKey) {
            if (message) {
                var textBox = $("<div class='msg-send'>" + message + "</div>");
                $('.msg-insert').append(textBox);
                textArea.val('');
                return false;
            } else {
                return false;
            }
        }
    });

    $('.my-select').select2({
        'data': JSON.parse(retrievedTheme)
        }
    );

    $('.apply-button').on('click', function() {
        var themeID = $('.my-select').val();
        $("body").css({
            'background': themes[themeID].bcgColor,
        });
        localStorage.setItem('selectedTheme', JSON.stringify(themes[themeID]));
    });

});

var print = document.getElementById('print');
var erase = false;

function go(x) {
    var temp = print.value;
    try {
        if (x === 'ac') {
            print.value = '';
        } else if (x === 'eval') {
            print.value = Math.round(evil(print.value) * 10000) / 10000;
            erase = true;
        } else if (x === 'sin') {
            temp *= (Math.PI / 180);
            print.value = Math.sin(temp);
            erase = true;
        } else if (x === 'tan') {
            temp *= (Math.PI / 180);
            print.value = Math.tan(temp);
            erase = true;
        } else if (x === 'log') {
            print.value = Math.log(temp);
            erase = true;
        } else {
            print.value += x;
        }
    } catch (e) {
        print.value = 'Error';
    }

}

function evil(fn) {
    return new Function('return ' + fn)();
}