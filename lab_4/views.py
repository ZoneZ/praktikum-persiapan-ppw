import pytz

from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_2.views import landing_page_content
from .forms import MessageForm
from .models import Message


response = {'author': 'ZoneZ'}
about_me = ['An everyday learner.',
            'Believes that simpler is better.',
            'Currently into web development, but also finds great interest in game development.',
            'A lone wolf.',
            'An old school when it comes to music preference.',
            'Mie Aceh FTW!'
            ]


def index(request):
    response['content'] = landing_page_content
    html = 'lab_4/lab_4.html'
    response['about_me'] = about_me
    response['messageform'] = MessageForm
    return render(request, html, response)


def message_post(request):
    form = MessageForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'],
                          email=response['email'],
                          message=response['message'])
        message.save()
        html = 'lab_4/form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/lab-4/')


def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'lab_4/table.html'
    return render(request, html, response)
