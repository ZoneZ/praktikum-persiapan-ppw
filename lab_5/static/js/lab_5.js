$(document).ready(function () {

    $('.to-do-list-delete-button').click(function () {
        var toDoListId = $(this).data('id');
        location.href = 'delete_todo/' + toDoListId;
    })
})