from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os


response = {}
csui_helper = CSUIhelper()


def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()
    response['mahasiswa_list'] = mahasiswa_list
    response['friend_list'] = friend_list
    html = 'lab_7/lab_7.html'
    return render(request, html, response)


def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'

    if request.is_ajax():
        friend_list = [obj.as_dict() for obj in friend_list]
        return JsonResponse({'friends': friend_list}, content_type='application/json')

    return render(request, html, response)


def friend_detail(request, friend_id):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    html = 'lab_7/detail_teman.html'
    mahasiswa = None
    for data in mahasiswa_list:
        if data['nama'] == Friend.objects.get(id=friend_id).friend_name:
            mahasiswa = data
            break

    if mahasiswa is not None:
        response['nama'] = mahasiswa['nama'].title()
        response['tgl_lahir'] = mahasiswa['tgl_lahir']
        response['alamat_mhs'] = mahasiswa['alamat_mhs']
        response['jurusan'] = mahasiswa['program'][0]['nm_org']
        return render(request, html, response)
    else:
        return HttpResponse("Data tidak ditemukan.")


'''def friend_list_json(request):
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({'results': friends}, content_type='application/json')'''


@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())


def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/')


@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)