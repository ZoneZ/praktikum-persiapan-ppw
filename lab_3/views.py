from datetime import datetime
from django.shortcuts import render, redirect, HttpResponse
from .models import Diary
import pytz
import json


def index(request):
    diary_dict = Diary.objects.all().values()
    return render(request, 'to_do_list.html',
                  {'diary_dict': convert_queryset_into_json(diary_dict)})


def add_activity(request):
    if request.method == 'POST':
        try:
            date = datetime.strptime(request.POST['date'], '%Y-%m-%dT%H:%M')
            act = request.POST['activity']
            Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),
                                 activity=act)
        except ValueError:
            return HttpResponse('Format tanggal salah')

    return redirect('/lab-3/')


def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val